#include <fstream>
#include <iostream>

#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

#include <mydatakeeper/arguments.h>
#include <mydatakeeper/exec.h>

#include <mydatakeeper/config.h>
#include <mydatakeeper/applications.h>
#include <mydatakeeper/systemd.h>

using namespace std;

// Child PID
pid_t pid;

// DNSmasq config files
string extra_conf_dir;
string config_file;
string hosts_file;
string resolv_file;

DBus::BusDispatcher dispatcher;

void forkOrReload()
{
    if (pid == 0) {
        clog << "Forking and launching dnsmasq process" << endl;
        pid = execute_async("/usr/bin/dnsmasq", {
            "-k",
            "--enable-dbus",
            "--user=dnsmasq",
            "--pid-file",
            "--filter-dbus=fr.mydatakeeper.filter",
            "--conf-file=" + config_file,
        });
        if (pid == -1) {
            exit(EXIT_FAILURE);
        }
    } else {
        clog << "Reloading dnsmasq process configuration" << endl;
        if (kill(pid, SIGHUP) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
    }
}

void killIfForked()
{
    if (pid != 0) {
        clog << "Killing openvpn process" << endl;
        if (kill(pid, SIGTERM) == -1) {
            perror("kill");
            exit(EXIT_FAILURE);
        }
        int status;
        if (waitpid(pid, &status, WUNTRACED | WCONTINUED) == -1) {
            perror("wait");
            exit(EXIT_FAILURE);
        }
        pid = 0;
    } else {
        clog << "No openvpn process to kill" << endl;
    }
}

int generateResolv(auto& dnsmasq)
{
    string dns1 = dnsmasq.Get(fr::mydatakeeper::ConfigInterface, "dns1");
    string dns2 = dnsmasq.Get(fr::mydatakeeper::ConfigInterface, "dns2");

    clog << "Writing resolv.conf file" << endl;
    std::ofstream ofs (resolv_file, std::ofstream::out | std::ofstream::trunc);

    ofs << "nameserver " << dns1 << endl;
    ofs << "nameserver " << dns2 << endl;

    ofs.close();

    return 0;
}

int generateConfig(auto& dnsmasq)
{
    generateResolv(dnsmasq);

    clog << "Writing config.conf file" << endl;
    std::ofstream ofs (config_file, std::ofstream::out | std::ofstream::trunc);

    /* System config */
    ofs << "interface=lan0" << endl;
    ofs << "port=53" << endl;
    ofs << "user=dnsmasq" << endl;
    ofs << "group=dnsmasq" << endl;
    ofs << "pid-file=/var/run/dnsmasq.pid" << endl;

    /* General config */
    ofs << "domain-needed" << endl;
    ofs << "bogus-priv" << endl;
    ofs << "filterwin2k" << endl;
    ofs << "cache-size=1000" << endl;
    ofs << "neg-ttl=3600" << endl;

    /* DNS config */
    ofs << "no-hosts" << endl;
    ofs << "resolv-file=" << resolv_file << endl;
    ofs << "expand-hosts" << endl;

    /* DHCP config */
    ofs << "log-dhcp" << endl;
    ofs << "dhcp-authoritative" << endl;
    ofs << "dhcp-range=interface:lan0,192.168.10.2,192.168.10.254,255.255.255.0,192.168.10.255,24h" << endl;
    ofs << "dhcp-range=interface:lan0,::1,constructor:eth0,ra-names,24h" << endl;
    ofs << "dhcp-lease-max=25" << endl;

    /* Include extra config files from other apps */
    ofs << "conf-dir=" << extra_conf_dir << endl;

    ofs.close();

    return 0;
}

inline void update(auto& dnsmasq)
{
    try {
        if (generateConfig(dnsmasq) == 0)
            forkOrReload();
    } catch (DBus::Error& e) {
        cerr << "An error occured during DNSmasq config update : " << e.message() << endl;
    }
}

void handler(int sig)
{
    clog << "Stopping DBus main loop (sig " << sig << ')' << endl;
    dispatcher.leave();
    killIfForked();
}

void bus_type(const string& name, const string& value)
{
    if (value != "system" && value != "session")
        throw invalid_argument(name + "must be either 'system' or 'session'");
}

int main(int argc, char** argv)
{
    string bus, bus_name, bus_path, conf_dir;
    mydatakeeper::parse_arguments(argc, argv, VERSION, {
        {
            bus, "bus", 0, mydatakeeper::required_argument, "system", bus_type,
            "The type of bus used by Dbus interfaces. Either system or session"
        },
        {
            bus_name, "bus-name", 0, mydatakeeper::required_argument, fr::mydatakeeper::ApplicationsName, nullptr,
            "The Dbus name which handles the configuration"
        },
        {
            bus_path, "bus-path", 0, mydatakeeper::required_argument, "/fr/mydatakeeper/app/dnsmasq", nullptr,
            "The Dbus path which handles the configuration"
        },
        {
            conf_dir, "conf-dir", 0, mydatakeeper::required_argument, "/var/lib/mydatakeeper-config/apps/fr.mydatakeeper.app.dnsmasq", nullptr,
            "The folder where configuration files will be generated"
        },
        {
            extra_conf_dir, "extra-conf-dir", 0, mydatakeeper::required_argument, "/var/etc/dnsmasq.d", nullptr,
            "The folder where extra configuration from other applications can be added"
        },
    });

    // Setup signal handler
    signal(SIGINT, handler);
    signal(SIGTERM, handler);

    // Setup DBus dispatcher
    DBus::default_dispatcher = &dispatcher;

    // Update variables depending on parameters
    DBus::Connection conn = (bus == "system") ?
        DBus::Connection::SystemBus() :
        DBus::Connection::SessionBus();

    pid = 0;

    config_file = conf_dir + "/dnsmasq.conf";
    hosts_file = conf_dir + "/hosts";
    resolv_file = conf_dir + "/resolv.conf";

    clog << "Getting Config " << bus_path << " proxy" << endl;
    fr::mydatakeeper::ConfigProxy dns(conn, bus_path, bus_name);

    clog << "Setting up Config " << bus_path << " update listener" << endl;
    dns.onPropertiesChanged = [&dns](auto& iface, auto&changed, auto& invalidated)
    {
        clog << "DNSmasq config has changed" << endl;
        update(dns);
    };

    // Force update during init
    update(dns);

    clog << "Starting DBus main loop" << endl;
    dispatcher.enter();

    return 0;
}
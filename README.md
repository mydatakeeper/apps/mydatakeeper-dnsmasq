
# Mydatakeeper Dnsmasq

## Presentation

This project aims to handle Dnsmasq configuration.

A list of available parameters can be found in the `manifest.json` file.

## Usage

### Prerequisites

You will need:

 * A modern C/C++ compiler
 * CMake 3.1+ installed

### Building The Project

```shell_session
$ git clone https://gitlab.com/mydatakeeper/apps/mydatakeeper-dnsmasq.git
$ cd mydatakeeper-dnsmasq
$ mkdir build
$ cd build
$ cmake ..
$ make -j8
```

### Installing the project

```shell_session
# make install
```

## Project Structure

There are two folders: `src` and `resources`. Each folder serves a self-explanatory purpose.

Source files are in `src`. Mydatakeeper application resources (a manifest and a logo) are in `resources`.

## Contributing

**Merge Requests are WELCOME!** Please submit any fixes or improvements, and I promise to review it as soon as I can at the project URL:

 * [Project Gitlab Home](https://gitlab.com/mydatakeeper/apps/mydatakeeper-dnsmasq)
 * [Submit Issues](https://gitlab.com/mydatakeeper/apps/mydatakeeper-dnsmasq/-/issues)
 * [Merge Requests](https://gitlab.com/mydatakeeper/apps/mydatakeeper-dnsmasq/-/merge_requests)

## License

&copy; 2019-2020 Mydatakeeper S.A.S.

Open sourced under GPLv3 license. See attached LICENSE file.
